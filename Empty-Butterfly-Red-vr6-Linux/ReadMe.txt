Empty Butterfly Red Cursor (vr6) for Linux.

Created by MOYASH.

------------------------

to install: 

1-Move "Empty-Butterfly-Red-vr6" folder to the ".icons" folder in your "Home" directory.

2-Then Choose and Apply the Cursor Name.

---------------------------------------------------

License:(CC BY-NC-ND).

---------------------------

Thanks for Downloading my Work.